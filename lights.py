import RPi.GPIO as GPIO
import time
import threading

class Lights(object):
    def __init__(self, blueLight, redLight, whiteLight1, whiteLight2):
        self.blueLight = blueLight # 23
        self.whiteLight1 = whiteLight1 # = 17
        self.whiteLight2 = whiteLight2 # = 22
        self.redLight = redLight # = 24

        GPIO.setmode(GPIO.BCM)
        
        GPIO.setup(blueLight, GPIO.OUT)     
        #GPIO.setup(whiteLight1, GPIO.OUT)
        #GPIO.setup(whiteLight2, GPIO.OUT)
        GPIO.setup(redLight, GPIO.OUT)

        self.showLights = True
        self.lightMode = 'A'
        self.t = threading.Thread(target=self.TurnOn)

    def LightModeA(self):
        print('Mode A: On')
        GPIO.output(self.blueLight, True)
        #GPIO.output(self.whiteLight1, True)
        #GPIO.output(self.whiteLight2, False)
        GPIO.output(self.redLight, False)
        time.sleep(.5)
        print('Mode A: Off')
        GPIO.output(self.blueLight, False)
        #GPIO.output(self.whiteLight1, False)
        #GPIO.output(self.whiteLight2, True)
        GPIO.output(self.redLight, True)
        time.sleep(.5)
    
    def LightModeB(self):
        print('Mode B: On')
        GPIO.output(self.blueLight, True)
        GPIO.output(self.redLight, True)
        #GPIO.output(self.whiteLight1, False)
        #GPIO.output(self.whiteLight2, Fase)
        time.sleep(.5)
        print('Mode B: Off')
        GPIO.output(self.blueLight, False)
        GPIO.output(self.redLight, False)
        #GPIO.output(self.whiteLight1, True)
        #GPIO.output(self.whiteLight2, True)
        time.sleep(.5)

    def TurnOff(self):
        print('Turn Off')
        GPIO.output(self.blueLight, False)
        GPIO.output(self.redLight, False)
        #GPIO.output(self.whiteLight1, False)
        #GPIO.output(self.whiteLight2, False)
        time.sleep(.5)
    

    def TurnOn(self):
        self.showLights = True
        
        while self.showLights:
            if self.lightMode == "A":
                self.LightModeA()

            if self.lightMode == "B":
                self.LightModeB()

            if self.lightMode == None:
                self.TurnOff()
                
    def Run(self):
        self.t.start()

    def Stop(self):
        self.showLights = False
        self.t.join()

    def joyButtonDown(self, event):
        if event.button == 4:
            self.lightMode = "A"
        elif event.button == 5:
            self.lightMode = "B"
        #else:
        #    self.lightMode = None

