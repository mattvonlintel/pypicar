import RPi.GPIO as GPIO
import time
import pygame
from pygame.locals import *
import motors as m
import lights as l

pygame.init()
screen = pygame.display.set_mode((64, 64))
pygame.joystick.init()
pygame.joystick.Joystick(0).init()

GPIO.setmode(GPIO.BCM)

motors = m.Motors(22, 27, 5, 6)
lights = l.Lights(18, 24, 17, 22)
lights.Run()

try:
    runnning = True
    lightMode = "A";
    while runnning:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.JOYBUTTONDOWN:
                lights.joyButtonDown(event)
                motors.joyButtonDown(event)
                
except KeyboardInterrupt:
    GPIO.cleanup()
    pygame.quit()

GPIO.cleanup()
pygame.quit()
