import RPi.GPIO as GPIO
import time
            
class Motors(object):

    def __init__(self, motor1forward, motor1backward, motor2forward, motor2backward):
        self.motor1forward = motor1forward
        self.motor1backward = motor1backward
        self.motor2forward =  motor2forward
        self.motor2backward = motor2backward        

        GPIO.setmode(GPIO.BCM)
        
        GPIO.setup(motor1forward, GPIO.OUT)
        GPIO.setup(motor1backward, GPIO.OUT)
        GPIO.setup(motor2forward, GPIO.OUT)
        GPIO.setup(motor2backward, GPIO.OUT)


    def DriveForward(self):
        GPIO.output(self.motor1forward, True)
        GPIO.output(self.motor2forward, True)
        GPIO.output(self.motor1backward, False)
        GPIO.output(self.motor2backward, False)
        time.sleep(1)
        self.Stop()
        
    def DriveBackward(self):
        print('drive backward')
        GPIO.output(self.motor1forward, False)
        GPIO.output(self.motor2forward, False)
        GPIO.output(self.motor1backward, True)
        GPIO.output(self.motor2backward, True)
        time.sleep(1)
        self.Stop()

    def TurnLeft(self):
        GPIO.output(self.motor1forward, True)
        GPIO.output(self.motor2forward, False)
        GPIO.output(self.motor1backward, False)
        GPIO.output(self.motor2backward, False)
        time.sleep(.5)
        self.Stop()

    def TurnRight(self):
        GPIO.output(self.motor1forward, False)
        GPIO.output(self.motor2forward, True)
        GPIO.output(self.motor1backward, False)
        GPIO.output(self.motor2backward, False)
        time.sleep(.5)
        self.Stop()

    def Stop(self):
        print('stop')
        GPIO.output(self.motor1forward, False)
        GPIO.output(self.motor2forward, False)
        GPIO.output(self.motor1backward, False)
        GPIO.output(self.motor2backward, False)

    def joyButtonDown(self, event):
        if event.button == 0:
            self.DriveForward()
        if event.button == 2:
            self.DriveBackward()
        elif event.button == 1:
            self.TurnRight()
        elif event.button == 3:
            self.TurnLeft()

